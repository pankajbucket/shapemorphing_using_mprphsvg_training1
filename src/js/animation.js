'use strict';
~ function () {
    var $ = TweenMax;

    window.init = function () {
 
        // MorphSVGPlugin.convertToPath("rect");
        // MorphSVGPlugin.convertToPath("circle");
        MorphSVGPlugin.convertToPath("polygon");

        var tl = new TimelineMax();
        tl.to("#evolution1",1,{morphSVG: "#evolution2"})
        tl.to("#evolution1",1,{morphSVG: "#evolution3"},"+=2")
        tl.to("#evolution1",1,{morphSVG: "#evolution4"},"+=2")

    }
   
}();
